package com.surindev.universityexam.repositories;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ReposContract {

    interface IReposModel{
        interface OnReposLoadedCallback{
            void onReposResponse(ArrayList<HashMap<String, String>> data);
            void onReposFailure(String errorMessage);
        }

        void getRepos(String token, OnReposLoadedCallback callback);
    }

    interface IReposPresenter{
        void detachView();
        void loadRepos(String token);

    }

    interface IReposView{
        void showToast(String message);
        void setData(ArrayList<HashMap<String, String>> data);
    }


    interface ReposApi {
        @GET("user/repos")
        Call<Object> getRepos(@Query("access_token") String access_token);
    }

}
