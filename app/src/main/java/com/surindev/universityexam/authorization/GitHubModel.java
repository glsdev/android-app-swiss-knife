package com.surindev.universityexam.authorization;

import com.surindev.universityexam.Application;
import com.surindev.universityexam.GitHubClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GitHubModel implements AuthContract.IGithubModel {

    @Override
    public void getAccessToken(String client_id, String clientSecret, String code, final GetAccesTokenCallback callback) {

        GitHubClient client = Application.getRetrofitGithub()
                .create(GitHubClient.class);

        Call<AccessToken> accessTokenCall = client.getAccessToken(
                client_id,
                clientSecret,
                code
        );

        accessTokenCall.enqueue(new Callback<AccessToken>() {
            @Override
            public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                callback.onResponseSuccess(response);
            }

            @Override
            public void onFailure(Call<AccessToken> call, Throwable t) {
                callback.onResponseFailure(t.toString());
            }
        });

    }
}
