package com.surindev.universityexam.authorization;

import retrofit2.Response;

import static com.surindev.universityexam.PrivateFieldsHolder.CLIENT_ID;
import static com.surindev.universityexam.PrivateFieldsHolder.CLIENT_SECRET;
import static com.surindev.universityexam.PrivateFieldsHolder.REDIRECT_URI;

public class AuthPresenter implements AuthContract.IAuthPresenter, AuthContract.IGithubModel.GetAccesTokenCallback {
    private AuthContract.IAuthView view;
    private AuthContract.IGithubModel model;


    public AuthPresenter(AuthContract.IGithubModel model) {
        this.model = model;
    }

    @Override
    public void attachView(AuthContract.IAuthView view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        view = null;
    }

    @Override
    public void onLoginClicked() {
        String token = view.getToken();
        if (token == null){
            view.authorizeUser(CLIENT_ID, REDIRECT_URI);
        }else{
            view.showMainActivity(token);
        }

    }

    @Override
    public void authSuccess(String code) {
        model.getAccessToken(CLIENT_ID, CLIENT_SECRET, code, this);
    }

    @Override
    public void authError() {
        view.showError();
    }

    @Override
    public String getRedirectUrl() {
        return REDIRECT_URI;
    }


    @Override
    public void onResponseFailure(String errorMessage) {
        if (view != null){
            view.showToast(errorMessage);
        }

    }

    @Override
    public void onResponseSuccess(Response<AccessToken> response) {
        if (response != null && response.body() != null) {
            if (view != null){
                String token = response.body().getAccessToken();
                view.saveToken(token);
                view.showMainActivity(token);
            }
        }else{
            onResponseFailure("Empty body");
        }
    }
}
