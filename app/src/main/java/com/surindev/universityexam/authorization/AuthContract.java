package com.surindev.universityexam.authorization;

import retrofit2.Response;

public interface AuthContract {

    interface IAuthView{
        void authorizeUser(String client_id, String redirect_url);
        String getToken();
        void saveToken(String token);
        void showMainActivity(String token);
        void showError();
        void showToast(String message);
    }

    interface IAuthPresenter{
        void attachView(IAuthView view);
        void detachView();
        void onLoginClicked();
        void authSuccess(String code);
        void authError();
        String getRedirectUrl();
    }

    interface IGithubModel{
        interface GetAccesTokenCallback{
            void onResponseFailure(String errorMessage);
            void onResponseSuccess(Response<AccessToken> response);
        }

        void getAccessToken(String clientId, String clientSecret, String code, GetAccesTokenCallback callback);
    }

}
