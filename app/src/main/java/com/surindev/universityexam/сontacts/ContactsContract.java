package com.surindev.universityexam.сontacts;

import java.util.List;
import java.util.Map;

public interface ContactsContract {

    interface IContractsView{
        void showContacts(List<Map<String, String>> contacts);
    }

    interface IContractsPresenter{
        void loadContacts(Object cr);
        void detachView();
    }

    interface IContractsModel{
        interface onContactsLoadCallback{
            void onContactsLoaded(List<Map<String, String>> maps);
        }

        void getContacts(Object cr, onContactsLoadCallback callback);
    }


}
