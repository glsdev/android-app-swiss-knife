package com.surindev.universityexam.main;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MainActivityContract {

    interface IMainModel{
        interface UserInfoCallback{
            void onUserInfoResponse(HashMap<String, String> user);
            void onUserInfoFailure(String errorMessage);
        }

        void requestUserInfo(String token, UserInfoCallback callback);
    }

    interface IMainPresenter{
        void detachView();
        void loadUserInfo(String token);
    }

    interface IMainView{
        void showToast(String text);
        void setUserInfo(String login, String url, String avatar_url);
    }

    interface UserInfoApi{

        @GET("user")
        Call<Object> getUserInfo(@Query("access_token") String access_token);

    }
}
