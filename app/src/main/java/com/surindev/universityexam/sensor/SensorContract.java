package com.surindev.universityexam.sensor;

import java.io.File;
import java.io.IOException;

public interface SensorContract {

    interface ISensorCameraView{
        void checkPermission();
        void openCamera(File file);
        void showToast(String message);
        File createImageFile() throws IOException;
        void scanMediaForImage();
    }

    interface ISensorCameraPresenter{
        void attachView(ISensorCameraView view);
        void detachView();
        void photoClick();
        void prepareCamera();
    }
}
